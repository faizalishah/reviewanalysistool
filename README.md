###The package contains the source code of the review analysis tool **REVSUM**, which demonstrates the following three main use cases:
- View users’ sentiments expressed towards app features 
- View which app features were mentioned in bug related reviews 
- View which new features were requested by users 

---

## Run Node.js server that receives REST API requests

- Go to folder "app-recommendator-api" and install required npm packages using the following commands:

	```
		cd app-recommendator-api
	```

	```
		npm install
	```

-  Start node.js server locally by running the following command:

	```
		npm start
	```


## Run Node.js web server

- Go to folder "app-recommendator-web" and install required npm packages by running the following commands:

	```
		cd app-recommendator-web
	```

	```
		npm install
	```

- Start node.js web server locally by running the following command:

	```
		npm start
	```

- The tool login page will be shown, enter username: demo and password: demo